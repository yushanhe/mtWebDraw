﻿/*
 * 全部修改重写，不保留原对比
 */
DiagramPanel = function(store, mainPanel)
{

    //grid 行展开显示内容
    var gridExpander_tpl = new Ext.Template(
            '<p class="textselect"><b>标识：</b>{ID}</p>',
            '<p><b>名称：</b>{DiagramName}</p>',
            '<p><b>流程图名称：</b>{ChartID:this.chartFormat}</p>',
            '<p><b>是否删除：</b>{IsDelete:this.boolFormat}</p>',
            '<p><b>创建时间：</b><br />{CreateTime:this.dateFormat}</p>',
            '<p><b>修改时间：</b><br />{UpdateTime:this.dateFormat}</p>',
            '<p><b>修改人：</b>{UpdateUser:this.userFormat}</p>',
            '<p><b>备注：</b>{Remark}</p>'
            );
    var gridExpander = new Ext.ux.grid.RowExpander({
        tpl: gridExpander_tpl
    });
    gridExpander_tpl.chartFormat = function (value, o) {
        return ChartInfo.cname;
    };
    gridExpander_tpl.dateFormat = function (value, o) {
        return value; //return Ext.util.Format.date(value, "Y-m-d H:i:s"); //Format不兼容IE11
    };
    gridExpander_tpl.boolFormat = function (data, o) {
        return data ? "是" : "否";
    };
    gridExpander_tpl.userFormat = function (data, o) {
        return GetUserDiaplayName(data);
    };

    _userLoadData(); //加载出用户下拉列表数据

    //显示数据表格
    var grid = new Ext.grid.GridPanel({
        id: 'diagramsGrid',
        title: 'Diagrams',
        iconCls: 'icons-chartdiagram',
	    scriptRow: true,
	    store: store,
	    viewConfig: {
	        firceFit: true,
	        deferEmptyText: false,
	        emptyText: '暂无Diagram数据！',
	        getRowClass: function (record, index) {
	            if (!(index % 2)) {
	                return 'x-grid3-row';
	            } else {
	                return 'x-grid3-row-alt';
	            }
	        }
	    },
	    columns: [
            new Ext.grid.RowNumberer(),
            gridExpander,
            { header: '标识', width: 60, dataIndex: 'ID', sortable: true, align: 'center', hidden: true },
            { header: '名称', width: 120, dataIndex: 'DiagramName', sortable: true, align: 'center' },
            {
                header: '流程图名称', width: 150, dataIndex: 'ChartID', sortable: true, align: 'center', hidden: true,
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    return ChartInfo.cname;
                }
            },
            { header: '已删除', width: 60, dataIndex: 'IsDelete', sortable: true, align: 'center', hidden: true, renderer: SetGridBoolValue },
            { header: '创建时间', width: 180, dataIndex: 'CreateTime', sortable: true, align: 'center', hidden: true },
            { header: '修改时间', width: 180, dataIndex: 'UpdateTime', sortable: true, align: 'center', hidden: true },
            { header: '修改人', width: 80, dataIndex: 'UpdateUser', sortable: true, align: 'center', hidden: true, renderer: GetUserDiaplayName },
            { header: '备注', width: 400, dataIndex: 'Remark', sortable: true, align: 'center', hidden: true }
	    ],
	    plugins: gridExpander,
	    sm: new Ext.grid.RowSelectionModel({ singleSelect: true }),
	    tbar: [{
	        text: '仅有效',
	        iconCls: 'preview-icon',
	        handler: function () {
	            DiagramStore.getNamesIsDelete = "0";
	            DiagramStore.eventSource.fireEvent(new mxEventObject('refresh'));
	        }
	    }, {
	        text: '仅删除',
	        iconCls: 'preview-icon',
	        handler: function () {
	            DiagramStore.getNamesIsDelete = "1";
	            DiagramStore.eventSource.fireEvent(new mxEventObject('refresh'));
	        }
	    }, {
	        text: '全部',
	        iconCls: 'preview-icon',
	        handler: function () {
	            DiagramStore.getNamesIsDelete = "2";
	            DiagramStore.eventSource.fireEvent(new mxEventObject('refresh'));
	        }
	    }],
	    listeners: {
	        'rowdblclick': function (grid, rowIndex, e) {
	            var ID = grid.getStore().getAt(rowIndex).get('ID');
	            var Name = grid.getStore().getAt(rowIndex).get('DiagramName');
	            mainPanel.openDiagram(ID, Name);
	        },
	        'rowcontextmenu': function (grid, rowIndex, e) {
	            var ID = store.getAt(rowIndex).get('ID');
	            var Name = store.getAt(rowIndex).get('DiagramName');

	            e.preventDefault();
	            var grid_menus = new Ext.menu.Menu({
	                shadow: 'sides',
	                items: [{
	                    text: '打开图形',
	                    iconCls: 'open-icon',
	                    handler: function () {
	                        mainPanel.openDiagram(ID, Name);
	                    }
	                }, {
	                    text: '重命名',
	                    iconCls: 'icons-base-edit',
	                    handler: function () {
	                        Ext.Msg.prompt('重命名', '请输入新名称：', function (btn, text) {
	                            if (btn == "ok") {
	                                DiagramStore.reName(ID, text, "");
	                            }
	                        }, this, false, Name); //false改为true则可多行输入
	                    }
	                }, {
	                    text: '设置备注',
	                    iconCls: 'icons-base-edit',
	                    handler: function () {
	                        Ext.MessageBox.show({
	                            width: 300,
	                            height: 150,
	                            title: '设置备注',
	                            msg: '请输入备注信息：',
	                            value: grid.getStore().getAt(rowIndex).get('Remark'),
	                            multiline: true,
	                            buttons: Ext.Msg.OKCANCEL,
	                            fn: function (btn, text) {
	                                if (btn == "ok") {
	                                    DiagramStore.reName(ID, "", text);
	                                }
	                            }
	                        });
	                    }
	                }, {
	                    text: '删除/恢复',
	                    iconCls: 'delete-icon',
	                    handler: function () {
	                        if (mxUtils.confirm('Delete "' + Name + '"?')) {
	                            DiagramStore.remove(ID);
	                        }
	                    }
	                }, {
	                    text: '修改历史',
	                    iconCls: 'preview-icon',
	                    handler: function () {
	                        OpenTabDiagramHistory(ID, Name, ChartInfo.cid, ChartInfo.cname);
	                    }
	                }]
	            });
	            grid_menus.showAt(e.getPoint());
	        }
	    }
	});
    
	return grid;

};
